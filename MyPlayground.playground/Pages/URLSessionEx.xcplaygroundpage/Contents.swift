//: [Previous](@previous)

import Foundation
let url = "https://jsonplaceholder.typicode.com/posts"

class BaseService {
    
    func fetchPosts(url: String, completion: @escaping (Result<[Posts], NetworkError>) -> Void) {
        guard let url = URL(string: url) else {
            completion(.failure(.domainError))
            return
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let _ = error {
                completion(.failure(.domainError))
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(.failure(.decodingError))
                return
            }
            guard let data = data else {
                completion(.failure(.decodingError))
                return
            }
            do {
                let posts = try JSONDecoder().decode([Posts].self, from: data)
                for post in posts {
                    print(post)
                }
                completion(.success(posts))
                
            } catch {
                completion(.failure(.decodingError))
            }
            
        }.resume()
    }
}



struct Posts: Codable {
    let userID, id: Int?
    let title, body: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}
enum NetworkError: String, Error {
    case domainError = "Invalid domain"
    case decodingError = "Decoding error"
}

func divide(_ x: Int, by y: Int, completion: (Result<Int, DivisionError>) -> Void) {
    guard y != 0 else {
        return completion(.failure(.zeroDivisor))
    }
    completion(.success(x / y))
}

divide(10, by: 0) { result in
    switch result {
    case .success(let number):
        print(number)
    case .failure(let error):
        print(error.localizedDescription)
    }
}

enum DivisionError: Error {
    case zeroDivisor
}

extension DivisionError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .zeroDivisor:
            return "Division by zero is quite problematic. " +
                   "(https://en.wikipedia.org/wiki/Division_by_zero)"
        }
    }
}
