import UIKit
import Alamofire

// MARK: - User
struct User: Codable {
    let id: Int?
    let name, username, email: String?
    let address: Address?
    let phone, website: String?
    let company: Company?
}

// MARK: - Address
struct Address: Codable {
    let street, suite, city, zipcode: String?
    let geo: Geo?
}

// MARK: - Geo
struct Geo: Codable {
    let lat, lng: String?
}

// MARK: - Company
struct Company: Codable {
    let name, catchPhrase, bs: String?
}

//Custom Keys
extension User {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case username
        case email
        case address
        case phone
        case website
        case company
    }
}

func fetchUserData() {
    let url = URL(string: "https://jsonplaceholder.typicode.com/users")
    AF.request(url!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
        .validate()
        .responseJSON { response in
            
            switch (response.result) {
            case .success( _):
                do {
                    let users = try JSONDecoder().decode([User].self, from: response.data!)
                    for user in users {
                        print(user.address)
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
    }
}

fetchUserData()

