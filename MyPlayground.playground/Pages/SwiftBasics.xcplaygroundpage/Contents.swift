//: [Previous](@previous)

import Foundation

//*********** Array ***********//
var arr:[Any] = [100, "Sunday", "d", true]
print(arr)


//*********** Sets ***********//

var arr1 = [1,2,3,4,5]
arr1[1]

// * Duplicate element not allowed
var set1:Set = [1,2,3,4,5,3,4,2,6]
set1.first

// * Convert Array to Sets - Automatically removes duplicate elements
var arr2 = [2,3,4,2,3,6,1,9,5,7]
var set2:Set = Set(arr2)

//Merge 2 sets
var set3:Set = [1,2,3,4]
var set4:Set = [3,4,5,6]

var set5 = set3.union(set4)

//*********** Dictionary ***********//

var alumnos = ["jose": 20, "leo": 56, "hadad": 8]
for (key, value) in alumnos {
   if value > 10 {
      print(key)
   }
}
