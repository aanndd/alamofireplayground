//: [Previous](@previous)

import Foundation

//1. Binary Search
func binarySearch(arr: [Int], key:Int) ->Int? {
    var lowerBound = 0
    var upperBound = arr.count
    while lowerBound < upperBound {
        let midIndex = lowerBound + (upperBound - lowerBound) / 2
        if arr[midIndex] == key {
            return midIndex
        } else if arr[midIndex] < key {
            lowerBound = midIndex + 1
        } else {
            upperBound = midIndex
        }
    }
    return nil
}

let numbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67]

binarySearch(arr: numbers, key: 41)

//2. Bubble Sort
func bubbleSort(arr: [Int])->[Int] {
    var tempArr = arr
    for i in 0..<tempArr.count {
        for j in 1..<tempArr.count-i {
            if tempArr[j] < tempArr[j-1]{
                let temp = tempArr[j-1]
                tempArr[j-1] = tempArr[j]
                tempArr[j] = temp
            }
        }
    }
    return tempArr
}

let arr = [3,9,7,5,1]
bubbleSort(arr: arr)


//3. FizzBuzz:
//For numbers divisible by 3, print “Fizz”
//For numbers divisible by 5, print “Buzz”
//For numbers divisible by both 3 and 5, print “FizzBuzz”
func fizzBuzz(num:Int){
    for i in 1...num
    {
        if i % 3 == 0 && i % 5 == 0 {
            print("\(i): FizzBuzz")
        } else if i % 3 == 0 {
            print("\(i): Fizz")
        } else if i % 5 == 0 {
            print("\(i): Buzz")
        } else {
            print(i)
        }
        
        switch (i % 3 == 0, i % 5 == 0)
        {
        case (true, false):
            print("\(i):: Fizz")
        case (false, true):
            print("\(i):: Buzz")
        case (true, true):
            print("\(i):: FizzBuzz")
        default:
            print(i)
        }
    }
}

fizzBuzz(num: 20)


// 4. Stack
struct stack {
    var arr = [Int]()
    
    var isEmpty:Bool {
        return arr.isEmpty
    }
    var count:Int {
        return arr.count
    }
    var top:Int? {
        return arr.last
    }
    mutating func push(element:Int) {
        arr.append(element)
    }
    mutating func pop() -> Int? {
        return arr.popLast()
    }
}
var st = stack()
st.isEmpty
st.count
st.push(element: 34)
st.push(element: 23)
st.top
st.count
st.push(element: 22)
st.count
st.pop()

//5. Selection Sort:Find the lowest number in the array.Swap the lowest number with the number at index 0.Go to index 1.Find the lowest number in the rest of the array.Swap the lowest number with the number at index 1 and so on.

func selectionSort(arr:[Int])->[Int] {
    guard arr.count > 1 else {
        return arr
    }
    var array = arr
    for i in 0..<array.count-1 {
        var lowest = i
        for j in i+1..<array.count {
            if array[j] < array[lowest]{
                lowest = j
            }
        }
        if i != lowest {
            array.swapAt(i, lowest)
        }
    }
    return array
}

let arr1 = [3,5,2,0,9,5]
selectionSort(arr: arr1)
