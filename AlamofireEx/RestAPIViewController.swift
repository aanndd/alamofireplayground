//
//  RestAPIViewController.swift
//  AlamofireEx
//
//  Created by Anand Yadav on 19/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

class RestAPIViewController: UIViewController {
    @IBOutlet weak var txtView: UITextView!
    var actionName:ActionName?
    override func viewDidLoad() {
        super.viewDidLoad()
        executeService(actionName: actionName!)
    }
    
    func executeService(actionName:ActionName) {
        
        switch actionName {
        case .allUser:
            ServiceManager.getUsersData(param: ["page":"2"], success: { (response) in
                
                let usersData = Users.map(JSONData:(response?.data)!)!
                self.txtView.text = "\(usersData)"
                print(usersData.data as Any)
                print(response?.response?.httpStatusCode as Any)
            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .userByID:
            ServiceManager.getUserDataById(param: "2", success: { (response) in
                
                let usersData = User.map(JSONData:(response?.data)!)!
                self.txtView.text = "\(usersData)"
                print(usersData)
                print(response?.response?.httpStatusCode as Any)
            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .createUser:
            let createUserParam = ["name":"Anand", "job":"Software Engineer"]
            ServiceManager.createUser(param: createUserParam, success: { (response) in
                
                let usersData = CreatedUser.map(JSONData:(response?.data)!)!
                self.txtView.text = "\(usersData)"
                print(usersData)
                print(response?.response?.httpStatusCode as Any)
            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .updateUser:
            let updateUserParam = ["name":"Anand", "job":"Software Engineer"]
            ServiceManager.updateUser(userID: "2", param: updateUserParam, success: { (response) in
                
                let usersData = CreatedUser.map(JSONData:(response?.data)!)!
                self.txtView.text = "\(usersData)"
                print(usersData)
                print(response?.response?.httpStatusCode as Any)
            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .deleteUser:
            ServiceManager.deleteUser(param: "2", success: { (response) in
                print(response?.response?.httpStatusCode as Any)
                self.txtView.text = "\(response)"

            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .registerUser:
            let registerUserParam = ["email":"anand.yadav@mastercard.com", "password":"password"]
            
            ServiceManager.registerUser(param: registerUserParam, success: { (response) in
                let usersData = CreatedUser.map(JSONData:(response?.data)!)!
                self.txtView.text = "\(usersData)"
                print(usersData)
                print(response?.response?.httpStatusCode as Any)
            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        case .loginUser:
            let loginUserParam = ["email":"anand.yadav@mastercard.com", "password":"password"]
            
            ServiceManager.login(param: loginUserParam, success: { (response) in
                print(response?.response?.httpStatusCode as Any)
                self.txtView.text = "\(response)"

            }) { (error) in
                if let error1 = error as? AFError {
                    print(error1.responseCode as Any)
                    print(error1.errorDescription as Any)
                }
            }
        }
    }
}
