//
//  Extensions.swift
//  AlamofireEx
//
//  Created by Anand Yadav on 18/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

extension Decodable {
    static func map(JSONData:Data) -> Self? {
        debugPrint(JSONData)

        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let utf8Data = String(decoding: JSONData, as: UTF8.self).data(using: .utf8)

            return try decoder.decode(Self.self, from: utf8Data!)
        } catch let error {
            print(error)
            return nil
        }
    }
}
