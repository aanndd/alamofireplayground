//
//  Users.swift
//  AlamofireEx
//
//  Created by Anand Yadav on 18/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct Users: Codable {
    let page, perPage, total, totalPages: Int?
    let data: [Datum]?

    enum CodingKeys: String, CodingKey {
        case page
        case perPage = "per_page"
        case total
        case totalPages = "total_pages"
        case data
    }
}

// MARK: - Datum
struct Datum: Codable {
    let id: Int?
    let email, firstName, lastName: String?
    let avatar: String?

    enum CodingKeys: String, CodingKey {
        case id, email
        case firstName = "first_name"
        case lastName = "last_name"
        case avatar
    }
}

// MARK: - Users
struct User: Codable {
    let data: Datum?
    let ad: Ad?
}

// MARK: - Ad
struct Ad: Codable {
    let company: String?
    let url: String?
    let text: String?
}

struct CreatedUser: Codable {
    let name, job, id, updatedAt, token, createdAt: String?
}
