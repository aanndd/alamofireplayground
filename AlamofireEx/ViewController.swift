//
//  ViewController.swift
//  AlamofireEx
//
//  Created by Anand Yadav on 07/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonClicked(_ sender: Any) {
        switch (sender as! UIButton).tag {
        case 1:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.allUser)
        case 2:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.userByID)
        case 3:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.createUser)
        case 4:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.updateUser)
        case 5:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.deleteUser)
        case 6:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.registerUser)
        case 7:
            performSegue(withIdentifier: "showRESTAPIVC", sender: ActionName.loginUser)
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let restAPIVC = segue.destination as! RestAPIViewController
        restAPIVC.actionName = sender as? ActionName
    }
}

enum ActionName: String {
    case allUser = "ALL_USER"
    case userByID = "USER_BY_ID"
    case createUser = "CREATE_USER"
    case updateUser = "UPDATE_USER"
    case deleteUser = "DELETE_USER"
    case registerUser = "REGISTER_USER"
    case loginUser = "LOGIN_USER"
}

