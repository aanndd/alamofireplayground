//
//  ServiceManager.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ServiceManager: NSObject {
    static func getUsersData(param: [String:Any],
                             success: @escaping (_ response: Results?) -> Void,
                             failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getUsers(parameters: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func getUserDataById(param: String,
                                success: @escaping (_ response: Results?) -> Void,
                                failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getUser(userID: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func createUser(param: [String:Any],
                           success: @escaping (_ response: Results?) -> Void,
                           failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.createUser(parameters: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func updateUser(userID:String, param: [String:Any],
                           success: @escaping (_ response: Results?) -> Void,
                           failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.updateUser(userID: userID, parameters: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func deleteUser(param: String,
                           success: @escaping (_ response: Results?) -> Void,
                           failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.deleteUser(userID: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func registerUser(param: [String:Any],
                             success: @escaping (_ response: Results?) -> Void,
                             failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getUsers(parameters: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
    
    static func login(param: [String:Any],
                      success: @escaping (_ response: Results?) -> Void,
                      failure: @escaping (_ error: Error?) -> Void) {
        BaseService().apiRequest(url: ServiceHelper.getUsers(parameters: param), success: { (response) in
            success(response)
        }) { (error) in
            failure(error)
        }
    }
}
