//
//  ServiceHelper.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

enum ServiceHelper: URLRequestConvertible {
    
    case getUsers(parameters: Parameters)
    case getUser(userID: String)
    case createUser(parameters: Parameters)
    case updateUser(userID: String, parameters: Parameters)
    case deleteUser(userID: String)
    case registerUser(parameters: Parameters)

    var method: HTTPMethod {
        switch self {
        case .getUsers:
            return .get
        case .getUser:
            return .get
        case .createUser:
            return .post
        case .updateUser:
            return .put
        case .deleteUser:
            return .delete
        case .registerUser:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .getUsers( _):
            return "/users"
        case .getUser(let userID):
            return "/users/\(userID)"
        case .createUser( _):
            return "/users"
        case .updateUser(let userID, _):
            return "/users/\(userID)"
        case .deleteUser(let userID):
            return "/users/\(userID)"
        case .registerUser( _):
            return "/register"
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .getUsers(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .getUser( _):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        case .createUser(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .updateUser( _, let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        case .deleteUser( _):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: nil)
        case .registerUser(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        debugPrint(urlRequest)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return urlRequest
    }
}

struct Response {
    var response: URLResponse?
    var httpStatusCode: Int = 0
    var headers = RestEntity()

    init(fromURLResponse response: URLResponse?) {
        guard let response = response else { return }
        self.response = response
        httpStatusCode = (response as? HTTPURLResponse)?.statusCode ?? 0
        
        if let headerFields = (response as? HTTPURLResponse)?.allHeaderFields {
            for (key, value) in headerFields {
                headers.add(value: "\(value)", forKey: "\(key)")
            }
        }
    }
}

struct Results {
    var data: Data?
    var response: Response?
    init(withData data: Data?, response: Response?) {
        self.data = data
        self.response = response
    }
}

struct RestEntity {
    private var values: [String: String] = [:]
    mutating func add(value: String, forKey key: String) {
        values[key] = value
    }
    func value(forKey key: String) -> String? {
        return values[key]
    }
    func allValues() -> [String: String] {
        return values
    }
    func totalItems() -> Int {
        return values.count
    }
}
