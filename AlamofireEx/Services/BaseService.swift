//
//  BaseService.swift
//  WeatherForecast
//
//  Created by Anand Yadav on 10/05/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit
import Alamofire

class BaseService: NSObject {
    
    func apiRequest(url:URLRequestConvertible, success: @escaping (_ response: Results?)-> Void, failure: @escaping (_ error:Error?)-> Void) {
        
        AF.request(url).validate().responseData { response in
            switch response.result {
            case .success(let data):
                print("Validation Successful")
                debugPrint(data)
                success(Results(withData: data, response: Response(fromURLResponse: response.response)))
                
            case .failure(let error):
                print(error)
                failure(error)
            }
        }
    }
}

